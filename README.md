# Swagger Generation Fault

In the example controller we have three `PUT` endpoints that return a `204` 
status.  They _do not_ return a `200` status.  However, the generated 
Swagger file incorrectly includes a `200` response in all cases.

This fault seems to have been introduced at Micronaut 2.2.0, version 
1.4.2 works correctly.