package co.uk.channele.test.swagger;

import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Put;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

@Controller
public class TestController {

    @Operation(responses = {@ApiResponse(responseCode = "204", description = "No Content")})
    @Put("/test")
    public HttpResponse<?> putOperation() {
        return HttpResponse.noContent();
    }

    @ApiResponse(responseCode = "204", description = "No content")
    @Put("/test2")
    public HttpResponse<?> putOperation2() {
        return HttpResponse.noContent();
    }

    @ApiResponses(value = {@ApiResponse(responseCode = "204", description = "No Content")})
    @Put("/test3")
    public HttpResponse<?> putOperation3() {
        return HttpResponse.noContent();
    }

}
